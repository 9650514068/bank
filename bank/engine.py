"""
    Version: To be filled as per the version to be released

    client.template.engine
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Implements the actual game logic
"""


from decimal import Decimal

from OGLE import OGLE_Slot
from OGA import EngineError
from OGA import OGAResponse_InitResponse, OGAResponse_Result

from . import defs
from .renderer import TemplateRenderer


class TemplateEngine(OGLE_Slot,TemplateRenderer):

    def __init__(self, **kwargs):
        super(TemplateEngine, self).__init__(defs, **kwargs)

    def init(self, request=None, storage=None):
        init_d = dict(
            defs=self.defs,
            info=self.defs.info,
            win_lines=self.defs.win_lines,
            symbols=self.defs.symbols,
            line_payouts=self.defs.payouts,
            scatter_wins=self.defs.scatter_wins,
        )
        return OGAResponse_InitResponse(result=dict(init=init_d,
                                                    parameter_list=self.get_parameters()))

    def play(self, request, storage):
        """ This method receives request components extracted by the parser, plus a storage dictionary.
            It returns an OGAResponse_Result object to tell OGA to deduct stake, credit
            winnings, whether to finish the gameplay and gamestate.

        :param request: dict: actual request send by the player to the engine
        :param storage: dict: storage for the game play
        :return: OGAResponse_Result of the valid feature requested
        """

        self.logger.info('New Request')

        # getting all the states from the storage for validation
        freespin_state = self.get_or_none(storage['gameplay'], 'freespin_state')
        triggering_state = self.get_or_none(storage['gameplay'], 'triggering_state')

        # Validate the request against the state: e.g. action='freespin': are freespins actually available?
        # And sequence of the request is valid, if not, raise invalid request error
        self.template_validate_request(request=request, freespin_state=freespin_state)

        # selection of any feature triggered or the regular base game to play if nothing triggered
        action = request['action']
        if action == 'spin':
            return self.base_game(request, storage,freespin_state,triggering_state)
        elif action == 'freespin':
            return self.freespin(request, storage, freespin_state, triggering_state)
        else:
            raise EngineError("Invalid action")

    def base_game(self, request, storage,freespin_state,triggering_state):
        """
         This method process all the base game. All the feature are triggered in this method
         and are stored in the storage as per the request by the player

        @param request: dict: actual request send by the player to the engine
        @param storage: dict: storage for all the gameplay
        @param freespin_state: freespin state of the game
        @param triggering_state: triggering state of the game
        :return: OGAResponse_Result method with all the winnings, stake due, gamestate
        """

        # attempt to retrieve the basegame values from request and defs
        selected_win_lines = range(defs.info['win_lines'])
        stake_per_line = request['stake_per_line'].amount
        currency = request['stake_per_line'].currency
        total_stake = stake_per_line * len(selected_win_lines)

        # variables initializing
        finish = True
        feature_trigger = None

        # getting the regular reel_sets
        reelset = self.choose_reelset('regular', defs.reel_sets)

        # Getting the reels and stops
        reels = reelset['reel']
        stop_list = self.spin(reels)

        # determine_symbols() builds a symbol grid as per the arguments given
        symbol_grid = self.determine_symbols(reelset=reels,
                                             stop_positions=stop_list,
                                             view_size=defs.info["view_size"])

        # process_factor_wilds builds a list of wilds
        factor_wilds = self.process_factor_wilds(symbol_grid=symbol_grid)

        # determine_line_wins builds a list of line wins or []
        line_wins = self.determine_line_wins(selected_win_lines=selected_win_lines,
                                             payouts=defs.payouts,
                                             symbol_grid=symbol_grid,
                                             stake=stake_per_line,
                                             factor_wild_list=factor_wilds)

        # winnings in this round
        current_winnings = sum([win['winnings'] for win in line_wins])

        # Total the winnings for this gamestate in current_winnings
        total_winnings = current_winnings

        # determine_scatter_wins returns a list of scatter wins or []
        scatter_wins = self.determine_scatter_wins(symbol_grid=symbol_grid,
                                                   payouts=defs.scatter_wins,
                                                   stake=total_stake,
                                                   )

        # checking for scatter wins
        if scatter_wins:
            # now a feature will be triggered so set finish to false
            finish = False
            for _wins in scatter_wins:
                # appending the feature name in the list which will be triggered in next spin
                feature_trigger = 'freespin'
                self.logger.info("feature_trigger: %s" % feature_trigger)

                if _wins['type'] == 'freespins':
                    freespin_state = {
                        'current_play': 0,
                        'num_free_spins': _wins["spins"],
                        'freespin_winnings': 0,
                    }

                    # storing the state for next spin
                    self.store(storage=storage['gameplay'], key='freespin_state', obj=freespin_state)
                    self.logger.info('freespin_available %r', freespin_state)

                    # this state will be give the necessary details that will allow to set the main game reels to
                    # their original state when the game go back from a bonus
                    triggering_state = {
                        'next_action': 'freespin',
                        'selected_win_lines': selected_win_lines,
                        'stake_per_line': stake_per_line,
                        'currency': currency,
                        'reelset': reelset['index'],
                        'stop_list': stop_list,
                        'symbol_grid': symbol_grid,
                        'scatter_wins': scatter_wins,
                        'total_winnings': current_winnings
                    }

                    # storing the triggering details, when a feature has been triggered
                    self.store(storage=storage['gameplay'], key='triggering_state', obj=triggering_state)
                else:
                    raise EngineError('scatter type Invalid')

        # call check_winnings_cap to check if total winnings so far exceed the cap
        # if they do, this function will finish the gameplay and cap the total_winnings
        total_winnings, finish, winnings_cap_applied = self.check_winnings_cap(total_winnings, finish)

        # in this game, winnings are credited only when the gameplay finishes
        if finish:
            winnings = total_winnings
        else:
            winnings = 0

        # the value of money that will be deducted from the wallet of player for this spin
        stake_due = total_stake

        # gamestate for rendering
        gamestate = {
            "request": request,  # the request object
            "selected_win_lines": selected_win_lines,  # list of win-line ids
            "currency": currency,  # currency of the gameplay
            "stake": total_stake,  # the total stake applied to the gamestate (not necessarily the stake to be debited)
            "stake_per_line": stake_per_line,  # stake per line applied to each win-line
            "finish": finish,  # bool: if this is final gamestate of the gameplay
            "stop_list": stop_list,  # list of stops: per reel
            "reelset": reelset['index'],  # id of reelset used
            "drawn_symbol_grid": symbol_grid,  # the originally drawn symbol grid
            "symbol_grid": symbol_grid,  # the symbol grid used to calculate wins (there could be overlays)
            "factor_wilds": factor_wilds,  # list of wilds with multiplier factors (factor could be 1.0)
            "overlay": [],  # overlay of symbol modification (expanding wild, sticky wild, etc.)
            "line_wins": line_wins,  # list of line wins
            "scatter_wins": scatter_wins,  # list of scatter wins
            "winnings_cap_applied": winnings_cap_applied,  # the cap value if applied - all other values are as drawn
            "freespin_state": freespin_state,  # the freespin state
            "current_winnings": current_winnings,  # win total for this gamestate
            "total_winnings": total_winnings,  # total winnings accumulated for this gameplay
            "win_amount": winnings,  # winnings to be credited to the wallet this gamestate
            "stake_due": stake_due,  # stake to be debited from the wallet this gamestate
            'feature_trigger': feature_trigger,  # triggered feature name
            'triggering_state': triggering_state,  # the triggering state
        }

        self.log_results(gamestate)

        return OGAResponse_Result(result=gamestate,
                                  stake=Decimal(stake_due),
                                  winning=Decimal(winnings),
                                  finish=finish)

    def freespin(self, request, storage, freespin_state, triggering_state):
        """ This method take care of the freespin feature of the game.

        :param request: dict: actual request send by the player to the engine
        :param storage: dict: storage for all the gameplay
        :return: OGAResponse_Result method with all the winnings, stake due, gamestate
        """

        # get selected win line, stake per line and currency from the freespin_state dict
        selected_win_lines = triggering_state['selected_win_lines']
        stake_per_line = triggering_state['stake_per_line']
        currency = triggering_state['currency']
        total_stake = stake_per_line * len(selected_win_lines)

        # variables initializing
        feature_trigger = None
        finish = False

        # getting the freespin reel_sets
        reelset = self.choose_reelset('freespin', defs.reel_sets)

        # Getting the reels and stops
        reels = reelset['reel']
        stop_list = self.spin(reels)

        symbol_grid = self.determine_symbols(reelset=reels,
                                             stop_positions=stop_list,
                                             view_size=defs.info["view_size"])

        # determine_scatter_wins returns a list of scatter wins or []
        scatter_wins = self.determine_scatter_wins(symbol_grid=symbol_grid,
                                                   payouts=defs.scatter_wins,
                                                   stake=total_stake,
                                                   )

        # Re-trigger mechanism of free spin
        # checking for scatter wins
        if scatter_wins:

            for _wins in scatter_wins:
                action = _wins['type']
                # adding number of spins, if free spins are re-triggered
                if action == 'freespins':
                    # choosing the number of freespins won by the player
                    num_free_spins = _wins['spins']
                    freespin_state['num_free_spins'] += num_free_spins

                    feature_trigger = 'freespinRetrigger'

        # determine_line_wins builds a list of line wins or []
        line_wins = self.determine_line_wins(selected_win_lines=selected_win_lines,
                                             symbol_grid=symbol_grid,
                                             payouts=defs.payouts,
                                             stake=stake_per_line,
                                             prefer_wild_wins=True
                                             )

        # Total the winnings for this gamestate in current_winnings
        current_winnings = sum([win['winnings'] for win in line_wins])

        freespin_state['current_play'] += 1
        freespin_state['num_free_spins'] -= 1
        freespin_state['freespin_winnings'] += current_winnings

        triggering_state['total_winnings'] += current_winnings
        total_winnings = triggering_state['total_winnings']

        # storing of the freespin state
        self.logger.info("freespin state: %r", freespin_state)
        self.logger.info("triggering_state: %r", triggering_state)

        self.store(storage=storage['gameplay'], key='freespin_state', obj=freespin_state)
        self.store(storage=storage['gameplay'], key='triggering_state', obj=triggering_state)

        if freespin_state['num_free_spins'] <= 0:
            finish = True

        # the value of money that will be deducted from the wallet of player for this spin
        stake_due = 0

        # call check_winnings_cap to check if total winnings so far exceed the cap
        # if they do, this function will finish the gameplay and cap the total_winnings
        total_winnings, finish, winnings_cap_applied = self.check_winnings_cap(total_winnings, finish)

        # in this game, we only credit winnings when we close the gameplay
        if finish:
            winnings = total_winnings
        else:
            winnings = 0

        # gamestate for rendering
        gamestate = {
            'request': request,  # the request object
            'currency': currency,  # currency of the gameplay
            'stake': total_stake,  # the total stake applied to the gamestate (not necessarily the stake to be debited)
            'stake_per_line': stake_per_line,  # stake per line applied to each win-line
            'selected_win_lines': selected_win_lines,  # list of win-line ids
            'finish': finish,  # bool: if this is final gamestate of the gameplay
            'stop_list': stop_list,  # list of stops: per reel
            'reelset': reelset['index'],  # id of reelset used
            'drawn_symbol_grid': symbol_grid,  # the symbol grid used to calculate wins (there could be overlays)
            'symbol_grid': symbol_grid,  # the original symbol grid
            'line_wins': line_wins,  # list of line wins
            'scatter_wins': scatter_wins,  # list of scatter wins
            'winnings_cap_applied': winnings_cap_applied,  # the cap value if applied - all other values are as drawn
            'freespin_state': freespin_state,  # the freespin state
            'current_winnings': current_winnings,  # win total for this gamestate
            'bonus_winnings': freespin_state['freespin_winnings'],  # win total for freespins
            'total_winnings': total_winnings,  # total winnings accumulated for this gameplay
            'win_amount': winnings,  # winnings to be credited to the wallet this gamestate
            'stake_due': stake_due,  # stake to be debited from the wallet this gamestate
            'triggering_state': triggering_state,  # triggering details from base game
            'feature_trigger': feature_trigger,  # for any new freespin is triggered
        }

        self.log_results(gamestate)

        return OGAResponse_Result(result=gamestate,
                                  stake=Decimal(stake_due),
                                  winning=Decimal(winnings),
                                  finish=finish)

    def template_validate_request(self, request, freespin_state):
        """
        We need to extend OGLE_Slot's validate_request to cover the additional action types of this game:
        feature_choice or gamble or both.
        First call the parent class method to check the regular actions (spin, freespin) but you must supply
        the additional actions for this engine to stop validate_request from rejecting them.

        For any problems, simply raise an exception of type EngineException and the gameplay will be rolled back.
        The error message will be passed to the client.
        """

        valid = OGLE_Slot.validate_request(self,
                                           request=request,
                                           freespin_state=freespin_state)

        return valid
